package edu.duke.risc_gui;

import edu.duke.risc_gui.Server.LimitChoice;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LimitChoiceTest {

    @Test
    void checkChoiceValidity() {
        LimitChoice limitChoice = new LimitChoice(20, "A");
        assertTrue(limitChoice.checkChoiceValidity(15));
        assertFalse(limitChoice.checkChoiceValidity(30));
    }

    @Test
    void getChoice() {
        LimitChoice limitChoice = new LimitChoice(20, "A");
        assertEquals("20", limitChoice.getChoice(20));
    }

    @Test
    void toText() {
        LimitChoice limitChoice = new LimitChoice(20, "A");
        assertEquals("Please enter number between 0 and 20", limitChoice.toText());
    }

    @Test
    void getChoiceBySocket() {
    }
}
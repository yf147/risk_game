package edu.duke.risc_gui;

import edu.duke.risc_gui.Server.Army;
import edu.duke.risc_gui.Server.Player;
import edu.duke.risc_gui.Server.Territory;
import org.junit.jupiter.api.Test;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class TerritoryTest {

    Territory t = new Territory("A");

    @Test
    void addNeighbor() {
        Territory t1 = new Territory("B");
        t.addNeighbor(t1);
        assertTrue(t.getNeighbors().contains(t1));
        assertNull(t.getLord());
    }

    @Test
    void setLord() {
        Player player = new Player(new Socket());
        t.setLord(player);
        assertEquals(player, t.getLord());
    }

    @Test
    void getNeighborNames() {
        assertEquals(new ArrayList<>(), t.getNeighborNames());
        Territory t1 = new Territory("B");
        t.addNeighbor(t1);
        assertEquals(new ArrayList<>(Arrays.asList("B")), t.getNeighborNames());
    }

    @Test
    void getUnit() {
        assertEquals(0, t.getUnit());
    }

    @Test
    void takenOver() {
        assertTrue(t.takenOver());
        t.attainUnit();
        assertFalse(t.takenOver());
    }

    @Test
    void loseUnit() {
        t.loseUnit();
        assertEquals(-1, t.getUnit());
    }

    @Test
    void attainUnit() {
        t.attainUnit();
        assertEquals(1, t.getUnit());
    }

    @Test
    void modifyUnit() {
        t.modifyUnit(5);
        assertEquals(5, t.getUnit());
    }

    @Test
    void startWar() {
        Player player = new Player(new Socket());
        Army army = new Army(2, player);
        t.addEnemy(army);
        Player lord = new Player(new Socket());
        t.setLord(lord);
        t.setUnit(10);
        t.startWar();
    }

    @Test
    void sendArmy() {
        Territory t1 = new Territory("B");
        t.sendArmy(t1, 0);
    }

    @Test
    void addEnemy() {
        Player player = new Player(new Socket());
        Army army = new Army(2, player);
        t.addEnemy(army);
    }

    @Test
    void getName() {
        assertEquals("A", t.getName());
    }

    @Test
    void setUnit() {
        t.setUnit(1);
        assertEquals(1, t.getUnit());
    }

    @Test
    void setName() {
        t.setName("B");
        assertEquals("B", t.getName());
    }

    @Test
    void toText() {
        Territory t1 = new Territory("B");
        t.addNeighbor(t1);
        Territory t2 = new Territory("C");
        t.addNeighbor(t2);
        t.toText();
        //assertEquals("\t0 units in A(next to: B, C)", t.toText());

    }
}

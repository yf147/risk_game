package edu.duke.risc_gui;

import edu.duke.risc_gui.Server.Player;
import edu.duke.risc_gui.Server.Territory;
import edu.duke.risc_gui.Server.World;
import org.junit.jupiter.api.Test;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlayerTest {
    public Socket socket = new Socket();
    public Player player = new Player(socket);


    @Test
    void getPlayerName() {
        Territory territory1 = new Territory("K");
        Territory territory2 = new Territory("P");
        player.addTerritory(territory1);
        player.addTerritory(territory2);
        player.setPlayerName("ttt");
        String name = player.getPlayerName();
        assertEquals(name,"ttt");
        ArrayList<String> names = player.getTerritoryNames();
        player.loseTerritory(territory1);
        player.isLost();
        Socket socket = player.getSocket();
        HashMap<String, Territory> territories = new HashMap<>();
        territories.put("K",territory1);
        territories.put("P",territory2);
        World world = new World(territories);
        player.setWorld(world);
        World world1 = player.getWorld();
        //ArrayList<String> test = player.filterEnemyTerritoriesByName("K");
        //ArrayList<String> test = player.filterEnemyTerritoriesByName("K");
        ArrayList<Territory> test3 = player.getTerritories();



    }

    @Test
    void addTerritory() {
        Territory territory = new Territory("N");
        player.addTerritory(territory);
    }

    @Test
    void toText() {
        player.toText();
    }

    @Test
    void getSocket() {
        socket = player.getSocket();
    }

    @Test
    void getTerritories() {
        ArrayList<Territory> t1 = player.getTerritories();

    }



}
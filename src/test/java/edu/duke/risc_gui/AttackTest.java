package edu.duke.risc_gui;

import edu.duke.risc_gui.Server.Attack;
import edu.duke.risc_gui.Server.Territory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AttackTest {

    @Test
    void getName() {
        Territory origin = new Territory("origin");
        Territory dest = new Territory("dest");
        Attack attack = new Attack(origin, dest, 10);
        assertEquals("Attack", attack.getName());
    }

    @Test
    void execute() {
        Territory origin = new Territory("origin");
        Territory dest = new Territory("dest");
        origin.setUnit(10);
        dest.setUnit(5);
        Attack attack = new Attack(origin, dest, 5);
        attack.execute();
        assertEquals(5, origin.getUnit());
        assertEquals(1, dest.enemyBuffer.size());
    }
}
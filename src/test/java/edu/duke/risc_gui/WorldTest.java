package edu.duke.risc_gui;

import edu.duke.risc_gui.Server.Move;
import edu.duke.risc_gui.Server.Player;
import edu.duke.risc_gui.Server.Territory;
import edu.duke.risc_gui.Server.World;
import org.junit.jupiter.api.Test;

import java.net.Socket;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WorldTest {
    Territory t1 = new Territory("A");
    Territory t2 = new Territory("B");
    HashMap<String, Territory> territories = new HashMap<String, Territory>();

    @Test
    void addPlayer() {
        territories.put("A", t1);
        territories.put("B", t2);
        Player player = new Player(new Socket());
        World world = new World(territories);
        world.addPlayer(player);
    }

    @Test
    void addAction() {
        territories.put("A", t1);
        territories.put("B", t2);
        World world = new World(territories);
        Move move = new Move(t1, t2, 0);
        world.addAction(move);
    }

    @Test
    void executeActions() {
        territories.put("A", t1);
        territories.put("B", t2);
        Player player = new Player(new Socket());
        World world = new World(territories);
        world.addPlayer(player);
        world.addAction(new Move(t1, t2, 0));
        world.executeActions();
    }

    @Test
    void toText() {
        territories.put("A", t1);
        territories.put("B", t2);
        Player player = new Player(new Socket());
        World world = new World(territories);
        world.addPlayer(player);
        assertEquals("null player:\n" + "------------\n" + "\n", world.toText());
    }

    @Test
    void getTerritoryByName() {
        territories.put("A", t1);
        territories.put("B", t2);
        World world = new World(territories);
        assertEquals(t1, world.getTerritoryByName("A"));
    }

}

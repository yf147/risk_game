package edu.duke.risc_gui;

import edu.duke.risc_gui.Server.SelectionChoice;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class SelectionChoiceTest {
    ArrayList<String> choices = new ArrayList<String>(Arrays.asList("apple", "banana", "orange"));
    SelectionChoice s = new SelectionChoice(choices, "M");

    @Test
    public void checkChoiceValidityTest() {
        assertFalse(s.checkChoiceValidity(-1));
    }

    @Test
    public void getChoiceTest() {
        assertNull(s.getChoice(-1));
        assertEquals("banana", s.getChoice(1));
    }

    @Test
    public void toTextTest() {
        assertEquals("\t0. apple\n" + "\t1. banana\n" + "\t2. orange\n", s.toText());
    }

    /*
    @Test
    public void getChoiceBySocketText() throws IOException {
        InputStream input = getClass().getClassLoader().getResourceAsStream("input.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(input));
        PrintStream ps = new PrintStream("output.txt");
        Socket socket = new Socket();
        Player player = new Player(socket);
        Message errorMsg = new Info("message");
        String actionChoiceName = s.getChoiceBySocket(br, ps, player, errorMsg);
        assertEquals("", actionChoiceName);
    }
     */
}

package edu.duke.risc_gui;

import edu.duke.risc_gui.Server.Move;
import edu.duke.risc_gui.Server.Territory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MoveTest {

    @Test
    void getName() {
        Territory sourceTerritory = new Territory("TTT");
        Territory destTerritory = new Territory("DDD");
        Integer unit = 1;
        Move move = new Move(sourceTerritory,destTerritory,unit);
        String a = move.getName();
        assertEquals("Move",a);
    }

    @Test
    void execute() {
        Territory sourceTerritory = new Territory("TTT");
        Territory destTerritory = new Territory("DDD");
        Integer unit = 2;
        Move move = new Move(sourceTerritory,destTerritory,unit);
        move.execute();

    }









}
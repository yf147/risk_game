package edu.duke.risc_gui;

import edu.duke.risc_gui.Server.Handler;
import edu.duke.risc_gui.Server.NumberHandler;
import org.junit.jupiter.api.Test;

class NumberHandlerTest {

    @Test
    void process() {
        Handler handler = new NumberHandler();
        handler.process("input");
        handler.process("123");

    }
}
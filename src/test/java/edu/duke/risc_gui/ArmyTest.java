package edu.duke.risc_gui;

import edu.duke.risc_gui.Server.Army;
import edu.duke.risc_gui.Server.Player;
import edu.duke.risc_gui.Server.Territory;
import org.junit.jupiter.api.Test;

import java.net.Socket;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {

    @Test
    void isDefeatedAndLostUnit() {
        Socket socket = new Socket();
        Army myArmy = new Army(1, new Player(socket));
        assertFalse(myArmy.isDefeated());
        myArmy.loseUnit();
        assertTrue(myArmy.isDefeated());
    }


    @Test
    void takeOver() {
        Socket socket = new Socket();
        Socket socket1 = new Socket();
        Player defender = new Player(socket);
        Player attacker = new Player(socket1);
        Territory t = new Territory("Land");
        t.setLord(defender);
        defender.addTerritory(t);
        t.getLord().setPlayerName("LandDefender");
        attacker.setPlayerName("LandAttacker");
        Army attackerArmy = new Army(10, attacker);
        attackerArmy.takeOver(t);
        assertEquals("LandAttacker", t.getLord().getPlayerName());
    }
}
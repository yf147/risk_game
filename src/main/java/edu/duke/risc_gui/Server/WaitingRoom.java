package edu.duke.risc_gui.Server;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class WaitingRoom extends Thread{
    private int playerNumber;

    public WaitingRoom(int playerNumber){
        this.playerNumber = playerNumber;
    }

    @Override
    public void run(){
        while (true){
            int sz = SocketManager.getSocketQueueSize();
            if (SocketManager.getSocketQueueSize()>=playerNumber){
                ArrayList<Socket> socketArrayList = new ArrayList<>();
                for (int i = 0; i < playerNumber; i++) {
                    socketArrayList.add(SocketManager.popSocket());
                }
                try {
                    ServerGamePlay serverGamePlay = new ServerGamePlay(playerNumber,socketArrayList);
                    serverGamePlay.start();
                    int x=0;
                    int u=0;
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

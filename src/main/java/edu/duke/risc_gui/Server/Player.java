package edu.duke.risc_gui.Server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The player that is playing the game
 */
public class Player {
    //player's socket
    private Socket socket;
    //player's name
    private String playerName;
    private HashMap<String, Territory> territories;
    private World world;

    /**
     * initialize
     * @param socket player's socket
     */
    public Player(Socket socket) {
        this.socket = socket;
        this.territories = new HashMap<>();
        this.world = null;
    }

    /**
     * get player's name
     * @return name
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * get all Territories name
     * @return territories
     */
    public ArrayList<String> getTerritoryNames() {
        return new ArrayList<>(territories.keySet());
    }

    /**
     * if the player lose this Territory, we will remove this Territory
     * @param t, the Territory that was lost
     */
    public void loseTerritory(Territory t) {
        assert (territories.get(t.getName()) != null);
        territories.remove(t.getName());
    }


    public void setPlayerName(String name) {
        this.playerName = name;
    }

    /**
     *  add this territory into the player's territories
     * @param territory, the adding territory
     */
    public void addTerritory(Territory territory) {
        territory.setLord(this);
        territories.put(territory.getName(), territory);
    }

    /**
     * print msg to player
     * @return msg
     */
    public String toText() {
        String res = this.playerName + " player:\n";
        res += "-".repeat(12);
        res += "\n";
        for (Territory t : territories.values()) {
            res += t.toText() + "\n";
        }
        return res;
    }

    /**
     * get socket
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * set world
     */
    public void setWorld(World world) {
        this.world = world;
    }

    /**
     * get world
     */
    public World getWorld() {
        return world;
    }

    /**
     * get territories
     */
    public ArrayList<Territory> getTerritories() {
        return new ArrayList<>(territories.values());
    }

    /**
     * when attack, use this method to filter Enemy's Territories
     * @param territoryName, source Territory Name
     * @return Enemy's Territories
     */
    public ArrayList<String> filterEnemyTerritoriesByName(String territoryName) {
        ArrayList<String> res = territories.get(territoryName).getNeighborNames();
        res.removeIf(name -> territories.containsKey(name));
        return res;
    }

    /**
     * check whether the player lost the game
     */
    public boolean isLost() {
        return territories.size() == 0;
    }
}

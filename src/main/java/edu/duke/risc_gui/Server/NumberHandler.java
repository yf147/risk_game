package edu.duke.risc_gui.Server;

/**
 * Number Handler
 */
public class NumberHandler implements Handler {
    @Override
    public Boolean process(String input) {
        try {
            Integer res = Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

package edu.duke.risc_gui.Server;

/**
 * info refers to message that does not require client to reply after print the msg
 */
public class Info implements Message {
    private String msg;
    //if the line contains this mark, then the player do not need to reply
    private String mark;
    public Info(String msg) {
        this.msg = msg;
        this.mark = "NOREPLY: ";
    }

    @Override
    public String generateSendMsg() {
        String separator = "=".repeat(30) + "\n";
        String res = mark + separator;
        String[] lines = msg.split(System.lineSeparator());
        for (String line : lines) {
            res += mark + line + "\n";
        }
        res += mark + separator;
        return res;
    }
}

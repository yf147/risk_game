package edu.duke.risc_gui.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerListening extends Thread{
    private ServerSocket serverSocket;
    public ServerListening(ServerSocket serverSocket){
        this.serverSocket = serverSocket;
    }

    @Override
    public void run(){
        while (true){
            try {
                Socket socket = serverSocket.accept();
                LogInCheckThread logInCheckThread = new LogInCheckThread(socket);
                logInCheckThread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

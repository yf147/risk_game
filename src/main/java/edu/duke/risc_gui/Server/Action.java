package edu.duke.risc_gui.Server;

/**
 * the player's action in one turn
 */
public interface Action {
    //get the action's name (i.e move, attack)
    String getName();
    //execute the action
    void execute();
}

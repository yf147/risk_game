package edu.duke.risc_gui.Server;

import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedDeque;

public class SocketManager {
    private static final ConcurrentLinkedDeque<Socket> socketQueue = new ConcurrentLinkedDeque<>();
    public static void addToSocketQueue(Socket socket){
        socketQueue.add(socket);
    }
    public static Socket popSocket(){
        return socketQueue.pollFirst();
    }

    public static int getSocketQueueSize(){
        return socketQueue.size();
    }

}

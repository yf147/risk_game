package edu.duke.risc_gui.Server;

/**
 * if there is no attackable Territory
 */
public class NoAttackableTerritoryException extends Exception{
    public NoAttackableTerritoryException(String s) {
        super(s);
    }
}

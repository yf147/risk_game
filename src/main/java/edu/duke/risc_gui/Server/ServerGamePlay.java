package edu.duke.risc_gui.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * The server that controls the game
 */
public class ServerGamePlay extends Thread{
    private World world;
    private final Integer playerCount;
    private final HashMap<String, Territory> territories;
    //all territories
    private final ArrayList<Territory> territoryArrayList;
//    private final ServerSocket serverSocket;
    //territoryNameBox contains all territories' name
    private final ArrayList<String> territoryNameBox;
    private final Integer eachPlayerTerritoryCount;

    private final ArrayList<Socket> playerSocketList;

    /**
     * add neighbors
     * @param t1, t2's neighbor
     * @param t2, t1's neighbor
     */
    private void addNeighbor(Territory t1, Territory t2) {
        t1.addNeighbor(t2);
        t2.addNeighbor(t1);
    }

    /**
     * initialize the whole World
     */
    private void initializeWorld() {
        // generate map
        Random random = new Random();
        //the total number of Territories
        int territoryNameLimit = eachPlayerTerritoryCount * playerCount;
        // create territories
        for (int i=0; i<territoryNameLimit; ++i) {
            Territory t = new Territory(territoryNameBox.get(i));
            territories.put(territoryNameBox.get(i), t);
            territoryArrayList.add(t);
        }
        // assign territories to the players
        for (int i=0; i<territoryNameLimit; ++i) {
            Territory t = territoryArrayList.get(i);
            int neighborCount = 0;
            while (neighborCount < 1) {
                neighborCount = random.nextInt(playerCount);
            }
            for (int j=0; j<neighborCount; ++j) {
                int neighborIndex;
                while ((neighborIndex = random.nextInt(territoryNameLimit)) == i) {}
                addNeighbor(t, territoryArrayList.get(neighborIndex));
            }
        }
    }

    /**
     * initialize Name Box
     */
    private void initializeNameBox() {
        territoryNameBox.add("Narnia");
        territoryNameBox.add("Midkemia");
        territoryNameBox.add("Oz");
        territoryNameBox.add("Elantris");
        territoryNameBox.add("Roshar");
        territoryNameBox.add("Scadrial");
        territoryNameBox.add("Gondor");
        territoryNameBox.add("Mordor");
        territoryNameBox.add("Hogwarts");
        territoryNameBox.add("Shanghai");
    }

    /**
     * set up the world
     */
    private void setUpWorld() throws InterruptedException, IOException {
        ArrayList<ClientHandler> chArray = new ArrayList<>();
        ArrayList<Player> players = new ArrayList<>();
        for (int i=0; i<playerCount; ++i) {
            Player player = new Player(playerSocketList.get(i));
            players.add(player);
            for (int j=0; j<eachPlayerTerritoryCount; ++j) {
                player.addTerritory(territoryArrayList.get(i*eachPlayerTerritoryCount+j));
            }
            ClientHandler ch = new ClientHandlerPlayerInit(player, 4);
            chArray.add(ch);
            ch.start();
        }
        for (int i=0; i<playerCount; ++i) {
            chArray.get(i).join();
            world.addPlayer(players.get(i));
        }
    }

    public ServerGamePlay(int playerCount,ArrayList<Socket> playerSocketList) throws IOException, InterruptedException {
        this.playerCount = playerCount;
        this.playerSocketList = playerSocketList;
        this.territories = new HashMap<>();
        this.territoryArrayList = new ArrayList<>();
        this.territoryNameBox = new ArrayList<>();
        initializeNameBox();
        this.eachPlayerTerritoryCount = territoryNameBox.size()/playerCount;
    }

    @Override
    public void run(){
        try {
            initializeWorld();
            this.world = new World(territories);
            setUpWorld();
            world.startWorld();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
}

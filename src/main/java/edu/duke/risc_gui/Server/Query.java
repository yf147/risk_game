package edu.duke.risc_gui.Server;

/**
 * info refers to message that needs client to reply after print the msg
 */
public class Query implements Message {
    String msg;
    //if the line contains this mark, then the player do not need to reply
    String mark;
    public Query(String msg) {
        this.msg = msg;
        this.mark = "NOREPLY: ";
    }

    @Override
    public String generateSendMsg() {
        String separator = "=".repeat(30) + "\n";
        String res = mark + separator;
        String[] lines = msg.split(System.lineSeparator());
        for (int i=0; i<lines.length; ++i) {
            res += "NOREPLY: " + lines[i] + "\n";
        }
        //the last line do not contain mark, so client need to reply
        res += separator + "\n";
        return res;
    }
}

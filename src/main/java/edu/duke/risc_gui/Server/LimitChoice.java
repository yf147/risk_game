package edu.duke.risc_gui.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * the player's choice is about units, i.e the number of units that he wants to send to fight.
 */
public class LimitChoice implements Choice{
    //the upper limit for player to choose
    private final Integer upperLimit;
    //the choice type, i.e number
    private final String type;

    /**
     * initialize
     */
    public LimitChoice(Integer upperLimit, String type) {
        this.upperLimit = upperLimit;
        this.type = type;
    }

    /**
     * check whether the choice is valid
     */
    @Override
    public boolean checkChoiceValidity(Integer i) {
        return i>=0 && i<=upperLimit;
    }

    /**
     * get choice
     */
    @Override
    public String getChoice(Integer i) {
        if (checkChoiceValidity(i)) {
            return ""+i;
        }
        return null;
    }

    @Override
    public String toText() {
        return "Please enter number between 0 and " + upperLimit;
    }

    /**
     * get the choice by socket
     */
    @Override
    public String getChoiceBySocket(BufferedReader br, PrintStream ps, Player player, Message errorMsg) throws IOException {
        String choiceName;
        while (true) {
            String prompt = "You are the \"" + player.getPlayerName() + "\" player, which " + type + " would you like to choose?\n";
            prompt += this.toText();
            Message chooseMsg = new Query(prompt);
            ps.print(chooseMsg.generateSendMsg());
            int choiceIndex;
            while (true) {
                try {
                    choiceIndex = Integer.parseInt(br.readLine());
                    break;
                } catch (NumberFormatException e) {
                    ps.print(errorMsg.generateSendMsg());
                }
            }
            //get choice name
            choiceName = this.getChoice(choiceIndex);
            if (choiceName == null) {
                ps.print(errorMsg.generateSendMsg());
            }
            else {
                break;
            }
        }
        return choiceName;
    }
}

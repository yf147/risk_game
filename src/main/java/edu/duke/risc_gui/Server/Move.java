package edu.duke.risc_gui.Server;

/**
 * play's action is to move units from source Territory to destTerritory
 */
public class Move implements Action {
    //source Territory
    private Territory sourceTerritory;
    //destTerritory
    private Territory destTerritory;
    //number of units that he wants to move
    private Integer unit;

    /**
     * initialize
     * @param sourceTerritory source Territory
     * @param destTerritory destTerritory
     * @param unit number of units that he wants to move
     */
    public Move(Territory sourceTerritory, Territory destTerritory, Integer unit) {
        this.sourceTerritory = sourceTerritory;
        this.destTerritory = destTerritory;
        this.unit = unit;
    }

    @Override
    public String getName() {
        return "Move";
    }

    @Override
    public void execute() {
        sourceTerritory.modifyUnit(-unit);
        destTerritory.modifyUnit(unit);
    }
}

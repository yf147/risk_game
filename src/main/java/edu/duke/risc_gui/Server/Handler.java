package edu.duke.risc_gui.Server;

public interface Handler {
    Boolean process(String input);
}

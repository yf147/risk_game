package edu.duke.risc_gui.Server;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

/**
 * Territory
 */
public class Territory {
    //number of unit in each Territory
    private int unit;
    //Territory's name
    private String name;
    //Territory's owner
    private Player lord;
    //Territory's neighbors
    private HashSet<Territory> neighbors;
    //store enemies
    public ArrayList<Army> enemyBuffer;

    /**
     * initialize territory
     * @param name, territory's name
     */
    public Territory(String name) {
        this.unit = 0;
        this.name = name;
        this.lord = null;
        this.neighbors = new HashSet<>();
        this.enemyBuffer = new ArrayList<Army>();
    }

    /**
     * add neighbot
     * @param t, neighbor
     */
    public void addNeighbor(Territory t) {
        neighbors.add(t);
    }

    /**
     * set the owner
     * @param p, owner
     */
    public void setLord(Player p) {
        this.lord = p;
    }

    /**
     * get all neighbor's name
     * @return names
     */
    public ArrayList<String> getNeighborNames() {
        ArrayList<String> res = new ArrayList<>();
        for (Territory t : neighbors) {
            res.add(t.getName());
        }
        return res;
    }

    /**
     * get lord
     */
    public Player getLord() {
        return lord;
    }

    /**
     * get units
     */
    public int getUnit() {
        return unit;
    }

    /**
     * if unit = 0, this territory is taken over
     */
    public boolean takenOver() {
        if (unit == 0) return true;
        return false;
    }

    public void loseUnit() {
        unit--;
    }
    public void attainUnit() {
        unit++;
    }

    public void modifyUnit(Integer i) {
        unit += i;
    }


    /**
     * fight
     * @param a, the army
     * @param t, territory
     */
    private void battle(Army a, Territory t) {
        Random random = new Random();
        while (!a.isDefeated() && !t.takenOver()) {
            int enemy = random.nextInt(20);
            int self = random.nextInt(20);
            if (enemy < self) {
                a.loseUnit();
            }
            else {
                t.loseUnit();
            }
        }
        if (!a.isDefeated()) {
            a.takeOver(t);
        }
    }

    /**
     * the army's info
     * @param unit, army's units
     * @return army
     */
    private Army formArmy(Integer unit) {
        this.unit -= unit;
        Army army = new Army(unit, lord);
        return army;
    }

    /**
     * start fight
     */
    public void startWar() {
        for (Army army : enemyBuffer) {
            battle(army, this);
        }
        enemyBuffer.clear();
    }

    /**
     * send army to dest
     * @param dest, dest Territory
     * @param unit, army's units
     */
    public void sendArmy(Territory dest, Integer unit) {
        dest.addEnemy(formArmy(unit));
    }


    /**
     * show text, i.e xx units in Territory A (next to : Territory B)
     * @return text
     */
    public String toText() {
        String res = "\t" + this.unit + " units in " + this.name + "(next to: ";
        int i = 0;
        for (Territory t : neighbors) {
            res += t.getName();
            ++i;
            if (i == neighbors.size()) break;
            res += ", ";
        }
        res += ")";
        return res;
    }

    /**
     * add enemy
     * @param army, army
     */
    public void addEnemy(Army army) {
        enemyBuffer.add(army);
    }

    public String getName() {
        return name;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashSet<Territory> getNeighbors() {
        return this.neighbors;
    }
}

package edu.duke.risc_gui.Server;

import java.util.ArrayList;

/**
 * Handler Chain
 */
public class HandlerChain {
    private ArrayList<Handler> handlers = new ArrayList<>();
    public void addHandler(Handler handler) {
        handlers.add(handler);
    }
    public boolean process(String input) {
        for (Handler handler : handlers) {
            if (!handler.process(input)) return false;
        }
        return true;
    }
}

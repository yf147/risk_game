package edu.duke.risc_gui.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;

/**
 * client handler, get the player's action
 */
public class ClientHandlerGetAction extends ClientHandler {
    //the whole world
    private World world;
    //error message output
    private Message errorMsg;
    //waiting message output
    private Message waitMsg;
    //all territory owned by the player and the number of units in each territory
    private HashMap<Territory, Integer> territoryUnitHashMap;

    /**
     * initialize ClientHandlerGetAction
     */
    public ClientHandlerGetAction(Player player) throws IOException {
        super(player);
        this.world = player.getWorld();
        this.errorMsg = new Info("Invalid Input. Please re-enter: ");
        this.waitMsg = new Info("Waiting for other players to react...");
        this.territoryUnitHashMap = new HashMap<>();
        for (Territory t : player.getTerritories()) {
            territoryUnitHashMap.put(t, t.getUnit());
        }
    }

    /**
     * get the current units
     */
    private Message getCurrentUnits() {
        String res = "Current unit situation: \n";
        for (Territory territory : territoryUnitHashMap.keySet()) {
            res += "\t" + territoryUnitHashMap.get(territory) + " units in " + territory.getName() + "\n";
        }
        return new Info(res);
    }

    /**
     * get the action
     */
    public Action getAction(BufferedReader br, PrintStream ps) throws IOException, NoAttackableTerritoryException {
        // print current unit situation
        ps.print(getCurrentUnits().generateSendMsg());
        //get the player's choice
        Choice actionChoice = new SelectionChoice(world.actionTypes, "action");
        //get the choice's name
        String actionChoiceName = actionChoice.getChoiceBySocket(br, ps, player, errorMsg);
        //check whether the play has done all actions
        if (actionChoiceName.equals("Done")) {
            return null;
        }
        //get the player's choice for sourceTerritory
        Choice sourceTerritoryChoice = new SelectionChoice(player.getTerritoryNames(), "source territory");
        //get the sourceTerritory's name
        String sourceTerritoryName = sourceTerritoryChoice.getChoiceBySocket(br, ps, player, errorMsg);
        String destTerritoryName = null;
        //get the destTerritory in different situation
        if (actionChoiceName.equals("Move")) {
            //get the move's destTerritory
            Choice destTerritoryChoice = new SelectionChoice(player.getTerritoryNames(), "destination territory");
            destTerritoryName = destTerritoryChoice.getChoiceBySocket(br, ps, player, errorMsg);
        }
        else if (actionChoiceName.equals("Attack")) { //if the play choose to attack
            //get the attack's destTerritory
            Choice destTerritoryChoice = new SelectionChoice(player.filterEnemyTerritoriesByName(sourceTerritoryName), "destination territory");
            destTerritoryName = destTerritoryChoice.getChoiceBySocket(br, ps, player, errorMsg);
        }
        if (destTerritoryName == null) {
            throw new NoAttackableTerritoryException("No territory to attack.");
        }
        //get the sourceTerritory
        Territory sourceTerritory = world.getTerritoryByName(sourceTerritoryName);
        //get the destTerritory
        Territory destTerritory = world.getTerritoryByName(destTerritoryName);
        //get the number of units that the player wants to send
        Choice unitChoice = new LimitChoice(territoryUnitHashMap.get(sourceTerritory), "number");
        Integer unit = Integer.parseInt(unitChoice.getChoiceBySocket(br, ps, player, errorMsg));
        territoryUnitHashMap.put(sourceTerritory, territoryUnitHashMap.get(sourceTerritory)-unit);
        //do different action in different situation
        Action action = null;
        if (actionChoiceName.equals("Move")) { //if the action is to move, put the units in destTerritory
            territoryUnitHashMap.put(destTerritory, territoryUnitHashMap.get(destTerritory)+unit);
            action = new Move(sourceTerritory, destTerritory, unit);
        }
        else if (actionChoiceName.equals("Attack")){ //if the action is to attack, do attack
            action = new Attack(sourceTerritory, destTerritory, unit);
        }

        return action;
    }

    //execute
    public void run() {
        try {
            // print New Round
            Message newRoundMsg = new Info("New Round");
            ps.print(newRoundMsg.generateSendMsg());
            // print world map
            String worldMap = world.toText();
            Message worldMapMsg = new Info(worldMap);
            ps.print(worldMapMsg.generateSendMsg());
            if (!player.isLost()) {
                //get actions from the player
                Action action = null;
                boolean exceptionThrown = false;
                try {
                    action = getAction(br, ps);
                } catch (NoAttackableTerritoryException e) {
                    ps.print(new Info("No adjacent territory is able to be attacked.").generateSendMsg());
                    exceptionThrown = true;
                }
                while (action != null || exceptionThrown) {
                    world.addAction(action);
                    try {
                        action = getAction(br, ps);
                        exceptionThrown = false;
                    } catch (NoAttackableTerritoryException e) {
                        ps.print(new Info("No adjacent territory is able to be attacked, please re-enter...").generateSendMsg());
                        exceptionThrown = true;
                    }
                }
                ps.print(waitMsg.generateSendMsg());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

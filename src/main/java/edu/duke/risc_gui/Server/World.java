package edu.duke.risc_gui.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * world
 */
public class World {
    //all players
    private ArrayList<Player> currentPlayers;
    //players that have lost the game
    private ArrayList<Player> losePlayers;
    private final HashMap<String, Territory> territories;
    //all actions
    private final ArrayList<Action> actions;
    //actionTypes, like move、attack
    public ArrayList<String> actionTypes;

    /**
     * initialize world
     * @param territories, the whole territories in the world
     */
    public World(HashMap<String, Territory> territories) {
        this.territories = territories;
        currentPlayers = new ArrayList<>();
        losePlayers = new ArrayList<>();
        actions = new ArrayList<>();
        actionTypes = new ArrayList<>();
        actionTypes.add("Move");
        actionTypes.add("Attack");
        actionTypes.add("Done");
    }

    /**
     * check whether the game is over
     * @return true for over, false for not
     */
    public boolean isGameOver() throws IOException {
        //traverse currentPlayers check for lose players
        for (Player player : currentPlayers) {
            if (player.isLost()) {
                String endMsg = "You lose.\nEnter \"shutdown\" to disconnect or wait until the end of the game\n";
                PrintStream ps = new PrintStream(player.getSocket().getOutputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(player.getSocket().getInputStream()));
                ps.print(new Query(endMsg).generateSendMsg());
                if (Objects.equals("shutdown", br.readLine())) {
                    ps.println("Goodbye");
                }
                losePlayers.add(player);
            }
        }
        //if the player is lost, remove from currentPlayers
        for (Player player : losePlayers) {
            currentPlayers.remove(player);
        }
        return currentPlayers.size() == 1;
    }

    /**
     * add player
     * @param p, player that needs to be added
     */
    public void addPlayer(Player p) {
        currentPlayers.add(p);
        p.setWorld(this);
    }


    /**
     * add action
     */
    public void addAction(Action action) {
        actions.add(action);
    }

    /**
     * add units for each Territory
     */
    private void addUnitForEachTerritory() {
        for (Territory territory : territories.values()) {
            territory.attainUnit();
        }
    }

    /**
     * start one round
     */
    public void startRound() throws InterruptedException, IOException {
        addUnitForEachTerritory();
        ArrayList<ClientHandler> clientHandlerArrayList = new ArrayList<>();
        for (Player player : losePlayers) {
            ClientHandler ch = new ClientHandlerGetAction(player);
            clientHandlerArrayList.add(ch);
            ch.start();
        }
        for (Player player : currentPlayers) {
            ClientHandler ch = new ClientHandlerGetAction(player);
            clientHandlerArrayList.add(ch);
            ch.start();
        }
        for (ClientHandler ch : clientHandlerArrayList) {
            ch.join();
        }
        executeActions();

    }

    /**
     * execute actions
     */
    public void executeActions() {
        // move
        for (Action action : actions) {
            action.execute();
        }
        // startWars
        for (Territory territory : territories.values()) {
            territory.startWar();
        }
        actions.clear();
    }


    public String toText() {
        String res = "";
        for (Player p : currentPlayers) {
            res += p.toText();
            res += "\n";
        }
        return res;
    }

    /**
     * get Territories by name
     * @param name,territory's name
     * @return name
     */
    public Territory getTerritoryByName(String name) {
        return territories.get(name);
    }

    /**
     * send win or lose message
     */
    private void sendWinMsg() throws IOException {
        for (Player player : losePlayers) {
            PrintStream ps = new PrintStream(player.getSocket().getOutputStream());
            ps.println(new Info("Game over. You lose.").generateSendMsg());
            ps.println("Goodbye");
            player.getSocket().close();
        }
        Player winner = currentPlayers.get(0);
        PrintStream ps = new PrintStream(winner.getSocket().getOutputStream());
        ps.println(new Info("You win.\n").generateSendMsg());
        ps.println("Goodbye");
        winner.getSocket().close();
    }

    /**
     * if game is not over, start new round
     */
    public void startWorld() throws InterruptedException, IOException {
        while (!isGameOver()) {
            startRound();
        }
        // send to the player, telling him he has won
        sendWinMsg();
    }
}

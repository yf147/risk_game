package edu.duke.risc_gui.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;

/**
 * player init
 */
public class ClientHandlerPlayerInit extends ClientHandler {
    //error msg output
    public Message errorMsg;
    //wait msg output
    public Message waitMsg;
    //the units in each Territory
    public int eachTerritoryUnit;

    /**
     * initialize
     * @param player, the player
     * @param eachTerritoryUnit, units in each Territory
     */
    public ClientHandlerPlayerInit(Player player, int eachTerritoryUnit) throws IOException {
        super(player);
        errorMsg = new Info("Invalid Input. Please Re-Enter.");
        waitMsg = new Info("Waiting for other players to react...");
        this.eachTerritoryUnit = eachTerritoryUnit;
    }

    /**
     * check whether the value is valid or not
     */
    private boolean checkValueValidity(ArrayList<Integer> unitValues, Integer totalUnits) {
        int sum = 0;
        for (int unit : unitValues) {
            if (unit < 0 || unit > totalUnits) return false;
            sum += unit;
        }
        return sum == totalUnits;
    }

    /**
     * set the player's name
     */
    public void setPlayerName(String name) throws IOException {
//        String name;
//        //tell player to input name
//        Message msg = new Query("Please enter your name:");
//        ps.print(msg.generateSendMsg());
//        while ((name = br.readLine()).length() == 0) {
//            ps.print(errorMsg.generateSendMsg());
//            ps.print(msg.generateSendMsg());
//        }
        player.setPlayerName(name);
    }

    /**
     * set Territory units
     */
    private void setTerritoryUnit(BufferedReader br, PrintStream ps) throws IOException {
        //get territories
        ArrayList<Territory> territories = player.getTerritories();
        //total units for player to assign
        int totalUnits = territories.size() * eachTerritoryUnit;
        String territoryNames = "";
        for (Territory territory : territories) {
            territoryNames += territory.getName() + " ";
        }
        //tell player to assign units to territories
        Message totalNumberMsg = new Info("You have " + totalUnits + " units in total, you need to assign them to your territories: " + territoryNames);
        ps.print(totalNumberMsg.generateSendMsg());
        //begin to assign
        while (true) {
            //get number of units
            ArrayList<Integer> unitValues = new ArrayList<>();
            for (int i=0; i<territories.size(); ++i) {
                Message enterNumberMsg = new Query("Please enter the unit number for " + territories.get(i).getName() + ": ");
                ps.print(enterNumberMsg.generateSendMsg());
                String unitValueString;
                while ((unitValueString = br.readLine()).length() == 0) {
                    ps.print(errorMsg.generateSendMsg());
                }
                //get unit value
                int unitValue;
                try {
                    unitValue = Integer.parseInt(unitValueString);
                } catch (NumberFormatException e) {
                    ps.print(errorMsg.generateSendMsg());
                    --i;
                    continue;
                }
                unitValues.add(unitValue);
            }
            //check whether the value is valid
            if (checkValueValidity(unitValues, totalUnits)) {
                for (int i=0; i<territories.size(); ++i) {
                    territories.get(i).setUnit(unitValues.get(i));
                }
                break;
            }
            else {
                ps.print(errorMsg.generateSendMsg());
            }
        }
        ps.print(waitMsg.generateSendMsg());
    }

    /**
     * execute
     */
    public void run() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(player.getSocket().getInputStream()));
            PrintStream ps = new PrintStream(player.getSocket().getOutputStream());
//            setPlayerName(br, ps);
            setTerritoryUnit(br, ps);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

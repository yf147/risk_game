package edu.duke.risc_gui.Server;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {
    private final ServerSocket serverSocket;
    private final int playerNum;
    public Server(int port,int playerNum) throws IOException {
        serverSocket = new ServerSocket(port);
        this.playerNum = playerNum;
    }

   public void serverListening(){
        ServerListening serverListening = new ServerListening(serverSocket);
        serverListening.start();
    }

    public void startWaitingRoom(){
        WaitingRoom waitingRoom = new WaitingRoom(playerNum);
        waitingRoom.start();
    }

    public static void main(String[] args) throws IOException {
        if(args.length!=3){
            System.err.println("you should input <ip> <portNum> <playerNum>");
            System.exit(-1);
        }
        int portNum = Integer.parseInt(args[1]);
        int playerNum = Integer.parseInt(args[2]);
        Server server = new Server(portNum,playerNum);
        server.serverListening();
        server.startWaitingRoom();
    }
}

package edu.duke.risc_gui.Server;

/**
 * the action is to attack
 */
public class Attack implements Action {
    //the source territory
    private Territory sourceTerritory;
    //the destination territory
    private Territory destTerritory;
    //the number of units sent over
    private Integer unit;

    //initialize attack action
    public Attack(Territory sourceTerritory, Territory destTerritory, Integer unit) {
        this.sourceTerritory = sourceTerritory;
        this.destTerritory = destTerritory;
        this.unit = unit;
    }

    //get the action's name
    @Override
    public String getName() {
        return "Attack";
    }

    //execute the action
    @Override
    public void execute() {
        sourceTerritory.sendArmy(destTerritory, unit);
    }
}

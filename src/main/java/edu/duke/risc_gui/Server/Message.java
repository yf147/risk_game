package edu.duke.risc_gui.Server;

/**
 * the message that sends to client
 */
public interface Message {
    public String generateSendMsg();
}

package edu.duke.risc_gui.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

/**
 * the player's choice is about selection, i.e he wants to move or attack
 */
public class SelectionChoice implements Choice{
    //the choices that can be chosen
    private ArrayList<String> choices;
    //the choice type, i.e source territory or dest territory or action
    private String type;

    /**
     * initialize SelectionChoice
     */
    public SelectionChoice(ArrayList<String> choices, String type) {
        this.choices = choices;
        this.type = type;
    }

    /**
     * check whether the choice is valid
     */
    @Override
    public boolean checkChoiceValidity(Integer i) {
        return i >= 0 && i < choices.size();
    }

    /**
     * get choice
     */
    @Override
    public String getChoice(Integer i) {
        if (checkChoiceValidity(i)) {
            return choices.get(i);
        }
        return null;
    }

    /**
     * show text
     */
    @Override
    public String toText() {
        String res = "";
        for (int i=0; i<choices.size(); ++i) {
            res += "\t" + i + ". " + choices.get(i) + "\n";
        }
        return res;
    }

    /**
     * get the choice by socket
     */
    @Override
    public String getChoiceBySocket(BufferedReader br, PrintStream ps, Player player, Message errorMsg) throws IOException {
        if (choices.size() == 0) return null;
        String choiceName;
        while (true) {
            String prompt = "You are the \"" + player.getPlayerName() + "\" player, which " + type + " would you like to choose?\n";
            prompt += this.toText();
            Message chooseMsg = new Query(prompt);
            ps.print(chooseMsg.generateSendMsg());
            int choiceIndex;
            String input = br.readLine();
            //check input
            HandlerChain handlerChain = new HandlerChain();
            handlerChain.addHandler(new NumberHandler());
            boolean res = handlerChain.process(input);
            if (res) {
                choiceIndex = Integer.parseInt(input);
            }
            else {
                ps.print(errorMsg.generateSendMsg());
                continue;
            }
            choiceName = this.getChoice(choiceIndex);
            if (choiceName == null) {
                ps.print(errorMsg.generateSendMsg());
            }
            else {
                break;
            }
        }
        return choiceName;
    }
}

package edu.duke.risc_gui.Server;

import java.util.concurrent.ConcurrentHashMap;

public class ClientNameAndPasswd {
    private static ConcurrentHashMap<String,String> nameAndPasswd = new ConcurrentHashMap<>();
    public static void setNameAndPasswd(String name,String passwd){
        nameAndPasswd.put(name,passwd);
    }
    public static String getPasswd(String name){
        return nameAndPasswd.get(name);
    }
}

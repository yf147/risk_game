package edu.duke.risc_gui.Server;

/**
 * Raise an army and send it to the enemy
 */
public class Army {
    //the number of units
    private Integer unit;
    //the army's lord
    private Player lord;

    //initialize the army
    public Army(Integer unit, Player lord) {
        this.unit = unit;
        this.lord = lord;
    }

    //check whether the army is defeated
    public boolean isDefeated() {
        if (unit == 0) return true;
        return false;
    }

    //lose unit
    public void loseUnit() {
        this.unit--;
    }

    //Send army into the enemy's territory
    public void takeOver(Territory t) {
        t.getLord().loseTerritory(t);
        t.setLord(lord);
        t.setUnit(unit);
        lord.addTerritory(t);
    }

}

package edu.duke.risc_gui.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * client handler, get the player and player's socket
 */
public abstract class ClientHandler extends Thread {
    protected Socket socket;
    protected Player player;
    protected BufferedReader br;
    protected PrintStream ps;

    //initialize the client handler
    public ClientHandler(Player player) throws IOException {
        super();
        this.socket = player.getSocket();
        this.player = player;
        this.br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.ps = new PrintStream(socket.getOutputStream());
    }
}

package edu.duke.risc_gui.Server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class LogInCheckThread extends Thread{
    private Socket socket;
    private BufferedReader br;
    private PrintStream ps;

    public LogInCheckThread(Socket socket) throws IOException {
        this.socket = socket;
        InputStream input = socket.getInputStream();
        br = new BufferedReader(new InputStreamReader(input));
        OutputStream out = socket.getOutputStream();
        ps = new PrintStream(out);
    }

    private void createAccount(String name,String passwd) throws IOException {
        if (ClientNameAndPasswd.getPasswd(name)!=null){
            ps.println("This name already existed");
        }else {
            ClientNameAndPasswd.setNameAndPasswd(name, passwd);
            //ClientHandlerPlayerInit.setPlayerName(name);
            ps.println("Create successfully");
        }
    }
    private boolean checkLogIn(String name,String passwd) throws IOException {
        if (ClientNameAndPasswd.getPasswd(name)==null){
            ps.println("cannot find user name");
            return false;
        }else if (!ClientNameAndPasswd.getPasswd(name).equals(passwd)){
            ps.println("wrong password");
            return false;
        }else {
            ps.println("logIn Success");
            return true;
        }
    }



    @Override
    public void run(){
        while (true){
            try {
                String receivedMsg = br.readLine();
                String[] logInInfo = receivedMsg.split("=");
                String flag = logInInfo[0];
                String name = logInInfo[1];
                String passwd = logInInfo[2];
                if (flag.equals("create")){
                    createAccount(name,passwd);
                }else {
                    if (checkLogIn(name,passwd)){
                        SocketManager.addToSocketQueue(socket);
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}

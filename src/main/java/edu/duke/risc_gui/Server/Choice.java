package edu.duke.risc_gui.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * get the player's choice
 */
public interface Choice {
    //check whether the choice is valid
    boolean checkChoiceValidity(Integer i);
    //get the choice
    String getChoice(Integer i);
    //show the text
    String toText();
    //get the choice by player's socket
    String getChoiceBySocket(BufferedReader br, PrintStream ps, Player player, Message errorMsg) throws IOException;
}

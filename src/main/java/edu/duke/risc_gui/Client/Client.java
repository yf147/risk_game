package edu.duke.risc_gui.Client;

import java.io.*;
import java.net.Socket;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Client {
    public static void main(String[] args) throws IOException, InterruptedException {
        Socket socket = new Socket("127.0.0.1", 8888);
        OutputStream out = socket.getOutputStream();
        PrintStream ps = new PrintStream(out);
        InputStream input = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(input));

        Scanner scanner = new Scanner(System.in);
        while (true) {
            // receive data
            String receivedMsg = br.readLine();
            if (Objects.equals(receivedMsg, "Goodbye")) {
                TimeUnit.SECONDS.sleep(1);
                break;
            }
            if (receivedMsg == null || receivedMsg.length() == 0) continue;
            if (receivedMsg.length() >= 7 && Objects.equals(receivedMsg.substring(0, 7), "NOREPLY")) {
                System.out.println(receivedMsg.substring(9));
            }
            else {
                System.out.println(receivedMsg);
                String msg = scanner.nextLine();
                ps.println(msg);
            }
        }
        scanner.close();
        socket.close();
    }
}

package edu.duke.risc_gui.Client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.*;
import java.net.Socket;

public class HelloApplication extends Application {
    ///
    @Override
    public void start(Stage stage) throws IOException {
        connectServer();
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 536, 410);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
    }

    private void connectServer() throws IOException {
        Socket socket = new Socket("127.0.0.1", 8888);
        ClientSocket.setSocket(socket);
    }

    public static void main(String[] args) {
        launch();
    }
}
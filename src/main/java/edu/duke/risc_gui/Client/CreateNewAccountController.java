package edu.duke.risc_gui.Client;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.*;
import java.util.Optional;

public class CreateNewAccountController {
    @FXML
    private TextField userName;
    @FXML
    private PasswordField passWord;

    @FXML
    private Button createNewAccountButton;
    @FXML
    protected void onCreateNewAccountButtonClick() throws IOException {
        String userNameStr = userName.getText();
        String passWordStr = passWord.getText();
        OutputStream out = ClientSocket.getSocket().getOutputStream();
        PrintStream ps = new PrintStream(out);
        String namePasswd = "create="+userNameStr+"="+passWordStr;
        ps.println(namePasswd);
        InputStream input = ClientSocket.getSocket().getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(input));
        String message = br.readLine();
        if (message.equals("Create successfully")){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.titleProperty().set("create status");
            alert.headerTextProperty().set("create successfully");
            Optional<ButtonType> clickButton = alert.showAndWait();
            if (clickButton.isPresent()&&clickButton.get()==ButtonType.OK){
                Stage stage = (Stage) createNewAccountButton.getScene().getWindow();
                stage.close();
            }
        }else {
            AlertHandler alertHandler = new AlertHandler();
            alertHandler.errorAlert("create status","This name already existed, create failed");
        }
    }

}

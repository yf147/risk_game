package edu.duke.risc_gui.Client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CreateNewAccount extends Application {


    @Override
    public void start(Stage stage) throws Exception {
        //connectServer();
        FXMLLoader fxmlLoader = new FXMLLoader(CreateNewAccount.class.getResource("createNewAccount-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 536, 410);
        stage.setTitle("create your account!");
        stage.setScene(scene);
        stage.show();
    }
}

package edu.duke.risc_gui.Client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GameStart extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(GameStart.class.getResource("gameStart-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 607, 506);
        stage.setTitle("game start");
        stage.setScene(scene);
        stage.show();
    }
}

package edu.duke.risc_gui.Client;

import javafx.scene.control.Alert;

public class AlertHandler {

    public void errorAlert(String status, String msg){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.titleProperty().set(status);
        alert.headerTextProperty().set(msg);
        alert.showAndWait();
    }

    public void successAlert(String status, String msg){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.titleProperty().set(status);
        alert.headerTextProperty().set(msg);
    }

}

package edu.duke.risc_gui.Client;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.*;
import java.net.Socket;

public class HelloController {

    @FXML
    private TextField userName;
    @FXML
    private PasswordField passWord;
    @FXML
    private Button logInButton;
    @FXML
    private Button newAccountButton;



    @FXML
    protected void onLogInButtonClick() throws Exception {
        String userNameStr = userName.getText();
        String passWordStr = passWord.getText();
        Socket socket = ClientSocket.getSocket();
        OutputStream out = socket.getOutputStream();
        PrintStream ps = new PrintStream(out);
        String namePasswd = "login="+userNameStr+"="+passWordStr;
        ps.println(namePasswd);
        InputStream input = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(input));
        String message = br.readLine();
        if (message.equals("cannot find user name")){
            AlertHandler alertHandler = new AlertHandler();
            alertHandler.errorAlert("Login status","cannot find user name");
        }else if(message.equals("wrong password")){
            AlertHandler alertHandler = new AlertHandler();
            alertHandler.errorAlert("Login status","wrong password");
        } else {
            AlertHandler alertHandler = new AlertHandler();
            alertHandler.successAlert("Login status","Login successfully");
            ///
            new GameStart().start(new Stage());
        }
    }

    @FXML
    protected void onNewAccountButtonClick() throws Exception {
        new CreateNewAccount().start(new Stage());
    }





















}
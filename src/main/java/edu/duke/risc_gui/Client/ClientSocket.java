package edu.duke.risc_gui.Client;

import java.net.Socket;

public class ClientSocket {
    private static Socket socket;

    public static Socket getSocket() {
        return socket;
    }

    public static void setSocket(Socket socket) {
        ClientSocket.socket = socket;
    }
}

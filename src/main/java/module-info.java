module edu.duke.risc_gui {
    requires javafx.controls;
    requires javafx.fxml;

    exports edu.duke.risc_gui.Client;
    opens edu.duke.risc_gui.Client to javafx.fxml;
}